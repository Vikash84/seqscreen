#!/usr/bin/env nextflow

params.help = false
params.fasta = ''
params.working = ''
params.log = '/dev/null'
params.slurm = false
params.sensitive = ''
mode = "fast"
Executor = 'local'

MODULES = "$workflow.projectDir/../modules"
SCRIPTS = "$workflow.projectDir/../scripts"

def usage() {
    log.info ''
    log.info 'Usage: nextflow run initialize.nf --fasta /Path/to/infile.fasta --working /Path/to/working_directory [--slurm] [--log=/Path/to/run.log]'
    log.info '  --fasta     Path to the input NT FASTA file'
    log.info '  --working   Path to the output working directory'
    log.info "  --sensitive Use SeqScreen sensitive mode (old default mode)"
    log.info '  --slurm     Submit modules in this workflow to run on a SLURM grid (Default = run locally)'
    log.info "  --log       Where to write log file (Default=no log)"
    log.info '  --help      Print this help message out'
    log.info ''
    exit 1
}

if (params.help) {
    usage()
}

if (params.slurm) {
    Executor = 'slurm'
}

if (!params.fasta) {
    log.error "Missing argument for --fasta option"
    usage()
}

if (!params.working) {
    log.error "Missing argument for --working option"
    usage()
}
if (params.sensitive) {
    mode = "sensitive"
}

fastafile = file(params.fasta)
workingDir = file(params.working)
logfile = file(params.log)

BASE=fastafile.getName()
WORKFLOW="initialize"

process Create_Working_Directories {
    output:
    stdout create

    """
    mkdir -p $workingDir
    """
}


process Verify_Fasta {
    input:
    val create from create

    output:
    stdout verify

    executor Executor

    """
    echo -n " # Launching $WORKFLOW workflow .................... " | tee -a $logfile; date '+%H:%M:%S %Y-%m-%d' | tee -a $logfile
    $SCRIPTS/validate_fasta.pl -f $fastafile --max_seq_size=1000000000
    """
}

create.subscribe { print "$it" }
verify.subscribe { print "$it" }
