#!/usr/bin/env nextflow

params.help = false
params.fasta = ''
params.working = ''
params.log = '/dev/null'
params.databases = ''
params.threads = 1
params.slurm = false
params.hmmscan = false
Executor = 'local'

def usage() {
    log.info ''
    log.info 'Usage: nextflow run initialize.nf --fasta /Path/to/infile.fasta --working /Path/to/working_directory --databases /Path/to/databases [--threads 4] [--slurm] [--log=/Path/to/run.log]'
    log.info '  --fasta     Path to the input NT FASTA file'
    log.info '  --working   Path to the output working directory'
    log.info "  --databases Path to the S2FAST databases"
    log.info "  --threads   Number of threads to use (Default=1)"
    log.info '  --slurm     Submit modules in this workflow to run on a SLURM grid (Default = run locally)'
    log.info '  --hmmscan   Run hmmscan on input sequences'
    log.info "  --log       Log file that the status can be tee'd to (Default=Don't save the log)"
    log.info '  --help      Print this help message out'
    log.info ''
    exit 1
}

if (params.help) {
    usage()
}

if (params.slurm) {
    Executor = 'slurm'
}

if (!params.fasta) {
    log.error "Missing argument for --fasta option"
    usage()
}

if (!params.working) {
    log.error "Missing argument for --working option"
    usage()
}

if (!params.databases) {
    log.error "Missing argument for --databases option"
    usage()
}


logfile = file(params.log)
fastafile = file(params.fasta)
workingDir = file(params.working)
databasesDir = file(params.databases)
pfamDir = file("$workingDir/seqmapper/pfam_hmm")
create_pfam = params.hmmscan

BASE = fastafile.getName()
MODULES =  "$workflow.projectDir/../modules"
SCRIPTS =  "$workflow.projectDir/../scripts"
THREADS = params.threads
WORKFLOW = "seqmapper"

bowtieDir = file("$workingDir/$WORKFLOW/bowtie2")
rapsearchDir = file("$workingDir/$WORKFLOW/rapsearch2")

translatedFile = "${pfamDir}/${BASE}.translated.fasta"

process Create_Working_Directories {
    output:
    stdout create

    """
    mkdir -p $workingDir
    mkdir -p $workingDir/$WORKFLOW

    if [ -d $bowtieDir ]; then rm -rf $bowtieDir; fi;
    if [ -d $rapsearchDir ]; then rm -rf $rapsearchDir; fi;
    if [ -d $pfamDir ]; then rm -rf $pfamDir; fi;

    mkdir $bowtieDir
    mkdir $rapsearchDir
    if $create_pfam; then mkdir $pfamDir; fi

    """
}

process Initialize {
    input:
    val create from create

    output:
    stdout init

    executor Executor

    """
    echo -n " # Launching $WORKFLOW workflow ........................ " | tee -a $logfile; date '+%H:%M:%S %Y-%m-%d' | tee -a $logfile
    """
}

process bowtie2 {
    input:
    val init from init

    output:
    stdout bowtie2

    if ( Executor == 'local' ) {
       executor "local"
    }

    else if ( Executor == 'slurm' ) {
       clusterOptions "--ntasks-per-node $THREADS"
       executor "slurm"
    }

    """
    ${MODULES}/bowtie2.sh --fasta=$fastafile \
        		  --database=${databasesDir}/bowtie2/blacklist.seqs.nt \
    			  --out=$bowtieDir/blacklist.sam \
    			  --threads=${THREADS}
    """
}

process rapsearch2 {
    input:
    val init from init

    output:
    stdout rapsearch2

    if ( Executor == 'local' ) {
       executor "local"
    }

    else if ( Executor == 'slurm' ) {
       clusterOptions "--ntasks-per-node $THREADS"
       executor "slurm"
    }

    """
    ${MODULES}/rapsearch2.sh --fasta=$fastafile \
    			     --database=${databasesDir}/rapsearch2/blacklist.seqs.aa \
    			     --out=$rapsearchDir/blacklist \
    			     --evalue=1e-9 \
    			     --threads=${THREADS}
    """
}

process pfam_hmm {
    if ( Executor == 'local' ) {
       executor "local"
    }

    else if ( Executor == 'slurm' ) {
       clusterOptions "--ntasks-per-node $THREADS"
       executor "slurm"
    }
    input:
    val init from init

    output:
    stdout pfam_hmm

    when:
    params.hmmscan

    script:
    """
    esl-translate  $fastafile > $translatedFile
    ${MODULES}/hmmscan.sh --fasta=$translatedFile \
    		 	  --database=${databasesDir}/hmmscan/Pfam-A.hmm \
    			  --out=$pfamDir/${BASE} \
    			  --threads=${THREADS} \
    			  --evalue=1e-3
    echo -n " # $WORKFLOW workflow complete ......................... " | tee -a $logfile; date '+%H:%M:%S %Y-%m-%d' | tee -a $logfile
    """
}


create.subscribe { print "$it" }
init.subscribe { print "$it" }
bowtie2.subscribe { print "$it" }
rapsearch2.subscribe { print "$it" }
pfam_hmm.subscribe { print "$it" }
